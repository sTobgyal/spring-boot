package com.item.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.item.itemModel.ItemModel;
import com.item.service.ItemService;


@RestController
public class MainController {

	@Autowired
	private ItemService itemservice;
	
	   @RequestMapping("/")
	   public String home() {
	         return "hello.html";
	     }
	
	   
	   @PostMapping("/saveItem")
	    public Object saveUser(@RequestBody ItemModel itemModel) {
	    	 return itemservice.saveitem(itemModel);
	    }
	   
	   
	   @GetMapping("/getItem/{id}")
	    public Object getsingledata(@PathVariable("id")int id){
	    	return itemservice.getItem(id);
	    }
	   
	   @GetMapping("/getItems")
	    public List<ItemModel> getAllUser(){
	    	return itemservice.getAllItems();
	    }
	   
		@PutMapping("/updateItem")
		public Object updateOrder(@RequestBody ItemModel itemModel) {
		return itemservice.UpdateItem(itemModel);
	}
		
			@DeleteMapping("/deleteOrder")
		public void deleteOrder(@RequestBody ItemModel itemModel) {
				itemservice.DeleteItem(itemModel);
	}
	
}
