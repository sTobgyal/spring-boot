package com.item.itemModel;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "item_receipt")
public class ItemModel {

	
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
	
	
    @Column(name = "received_id")
    private BigInteger received_id;
	
    @Column(name = "stock_id")
    private BigInteger stock_id;
    
    @Column(name = "received_qty")
    private Integer received_qty;
    
    @Column(name = "rate")
    private Double rate;
    
    @Column(name = "amount")
    private Double amount;
    
    @Column(name = "balance_qty")
    private Integer balance_qty;
    
    @Column(name = "receipt_no")
    private BigInteger receipt_no;
    
    
    
    public ItemModel() {
    	super();
    }



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public BigInteger getReceived_id() {
		return received_id;
	}



	public void setReceived_id(BigInteger received_id) {
		this.received_id = received_id;
	}



	public BigInteger getStock_id() {
		return stock_id;
	}



	public void setStock_id(BigInteger stock_id) {
		this.stock_id = stock_id;
	}



	public Integer getReceived_qty() {
		return received_qty;
	}



	public void setReceived_qty(Integer received_qty) {
		this.received_qty = received_qty;
	}



	public Double getRate() {
		return rate;
	}



	public void setRate(Double rate) {
		this.rate = rate;
	}



	public Double getAmount() {
		return amount;
	}



	public void setAmount(Double amount) {
		this.amount = amount;
	}



	public Integer getBalance_qty() {
		return balance_qty;
	}



	public void setBalance_qty(Integer balance_qty) {
		this.balance_qty = balance_qty;
	}



	public BigInteger getReceipt_no() {
		return receipt_no;
	}



	public void setReceipt_no(BigInteger receipt_no) {
		this.receipt_no = receipt_no;
	}



	
    
    
    
}
