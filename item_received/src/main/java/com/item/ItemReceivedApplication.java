package com.item;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItemReceivedApplication {

	public static void main(String[] args) {
		SpringApplication.run(ItemReceivedApplication.class, args);
	}

}
