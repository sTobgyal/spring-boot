package com.item.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.item.itemModel.ItemModel;

public interface ItemRepository extends JpaRepository<ItemModel, Integer> {

}
