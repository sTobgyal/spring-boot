package com.item.service;

import java.util.ArrayList;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.item.itemModel.ItemModel;
import com.item.repo.ItemRepository;
@Service
public class ItemService {

	
	@Autowired
	private ItemRepository itemrepo;
	
	public Object saveitem(ItemModel itemModel) {
		return itemrepo.save(itemModel);
	}
	
	
	public List<ItemModel> getAllItems() {
		List<ItemModel> helloModel = new ArrayList<>();	
		itemrepo.findAll().forEach(helloModel::add);
		 return helloModel;
	}
	
	
	public Object getItem(int id) {
		return itemrepo.findById(id);
	}
	
	
	
	public Object UpdateItem(ItemModel itemModel) {
		return itemrepo.save(itemModel);
	}

	public void DeleteItem(ItemModel itemModel) {
		itemrepo.delete(itemModel);
	}
	
	
}
