package net.inventory.bt.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import net.inventory.bt.model.User;
import net.inventory.bt.web.dto.UserRegistrationDto;

public interface UserService extends UserDetailsService {
	User save(UserRegistrationDto registrationDto);
}
